package db

import (
	"github.com/jmoiron/sqlx"
)

func CreateMySQLDefault(connectionString string) (db *sqlx.DB,err error) {
	return Create(MySQL,connectionString,150,16)
}
//创建数据连接
//@dbType 数据库驱动类型
//@connectionString 数据库连接字符串
//@maxIdleConns  最大连接数
//@maxOpenConns  当无连接时，最大的保持连接的连接数
func Create(dbType string,connectionString string,maxIdleConns int,maxOpenConns int) (db *sqlx.DB,err error) {
	db, err = sqlx.Open(dbType, connectionString)
	if err != nil {
		return
	}
	err=db.Ping()
	db.SetMaxIdleConns(maxIdleConns)
	db.SetMaxOpenConns(maxOpenConns)
	return
}

const (
	MySQL="mysql"
	SQLServer="mssql"
	Oracle="oracle"
	PostgreSQL="postgres"
	SQLite="sqlite3"
)
