module gitee.com/yunduansing/gocommon

go 1.14

require (
	github.com/Joker/hpp v1.0.0 // indirect
	github.com/go-redis/redis/v8 v8.7.1
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/gomodule/redigo v1.7.1-0.20190724094224-574c33c3df38
	github.com/hashicorp/consul/api v1.8.1
	github.com/iris-contrib/middleware/jwt v0.0.0-20210110101738-6d0a4d799b5d
	github.com/jmoiron/sqlx v1.2.0
	github.com/k0kubun/colorstring v0.0.0-20150214042306-9440f1994b88 // indirect
	github.com/kataras/iris/v12 v12.2.0-alpha2
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/segmentio/kafka-go v0.4.12
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/streadway/amqp v1.0.0
	github.com/yudai/pp v2.0.1+incompatible // indirect
	google.golang.org/grpc v1.27.0
	gopkg.in/yaml.v2 v2.4.0
)
