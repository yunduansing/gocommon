package grpc

import (
	"gitee.com/yunduansing/gocommon/consul"
	"google.golang.org/grpc"
	"google.golang.org/grpc/grpclog"
	"google.golang.org/grpc/health/grpc_health_v1"
	"net"
	"strconv"
)
//获取grpc连接对象
func GetConn(serviceAddr string) (*grpc.ClientConn,error) {
	// 连接
	conn, err := grpc.Dial(serviceAddr, grpc.WithInsecure())
	if err != nil {
		grpclog.Fatalln(err)
	}
	return conn,err
}

func RegisterGrpc(serviceRegisterAddr string,server *grpc.Server,port int,ip string,serviceName string) error {
	listen, err := net.Listen("tcp", ip+":"+strconv.Itoa(port))
	if err != nil {
		grpclog.Fatalf("Failed to listen: %v", err)
	}
	grpc_health_v1.RegisterHealthServer(server,&HealthImpl{
		Status: grpc_health_v1.HealthCheckResponse_SERVING,
	})
	consul.Register(serviceRegisterAddr,ip,port,serviceName,"")
	grpclog.Infoln("Listen on " + serviceRegisterAddr)
	err=server.Serve(listen)
	return err
}
