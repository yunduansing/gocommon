package irisweb

import "github.com/kataras/iris/v12"

func Init(addr string,config iris.Configurator) (app *iris.Application,err error) {
	app=iris.Default()
	err=app.Run(iris.Addr(addr),config)
	return
}
