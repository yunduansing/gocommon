package main

import (
	"fmt"
	"gitee.com/yunduansing/gocommon/configs"
	"gitee.com/yunduansing/gocommon/consul"
	"gitee.com/yunduansing/gocommon/redis"
)

func main()  {
	err:=consul.Register("localhost:8500","localhost",6080,"x1","")
	fmt.Println(err)
}

func testRedisLock()  {
	r,err:=redis.Create("localhost:6379")
	if err!=nil{
		fmt.Println(err)
		return
	}
	lock:=redis.GetLock(r,"testLockkey","1",30000)
	r1,l1,err:=lock.Lock()
	fmt.Println(r1,l1,err)
	//r2,l2,err:=redis.CreateDistLock(r,"testLockkey","2",30000)
	//fmt.Println(r2,l2,err)
	//result,err:=redis.UnDistLock(r,"testLockkey","1")
	//if err!=nil{
	//	fmt.Println(err)
	//	return
	//}
	//fmt.Println(result)
}

//func testEtcdRegister()  {
//	err:=etcd.Register("http://localhost:2379","127.0.0.1",8881)
//	fmt.Println(err)
//}

//func testEtcdGetGrpcConn()  {
//	_,err:=etcd.GetGrpcConn("http://localhost:2379","iristestGrpc")
//	fmt.Println(err)
//}

func getConsulServiceTest()  {
	services,err:=consul.GetService("localhost:8500","consul")
	if err!=nil{
		fmt.Println("Gets service error:",err)
	}
	fmt.Println(services)
}

func testGetConfig()  {
	cfg,_:=configs.GetYAMLConfig("./cfg.yml")

	mysql,err:=configs.GetConfig(cfg,"mysql.host")
	if err!=nil{
		fmt.Println(err)
	}else {
		fmt.Println(mysql)
	}
	redis,err:=configs.GetConfig(cfg,"redis.host")
	if err!=nil{
		fmt.Println(err)
	}else {
		fmt.Println(redis)
	}
}
