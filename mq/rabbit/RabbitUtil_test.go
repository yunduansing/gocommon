package rabbit

import (
	"fmt"
	"github.com/streadway/amqp"
	"testing"
	"time"
)

//func TestRabbitMQ_PublishSimple(t *testing.T) {
//	r,err:=NewRabbitMQSimple("amqp://guest:guest@127.0.0.1:5672/","test")
//	if err!=nil{
//		t.Fatal(err)
//	}
//	fmt.Println("publish message.")
//	err=r.PublishSimple("test message")
//	if err!=nil{
//		t.Fatal(err)
//	}
//}
//
//func TestRabbitMQ_ConsumeSimple(t *testing.T) {
//	forever := make(chan bool)
//	go genConsumeSimple(1)
//	go genConsumeSimple(2)
//	<-forever
//}
//
//func genConsumeSimple(i int)  {
//	r,err:=NewRabbitMQSimple("amqp://guest:guest@127.0.0.1:5672/","test")
//	if err!=nil{
//		fmt.Println(i,err,time.Now())
//	}
//	forever := make(chan bool)
//	err=r.ConsumeSimple(func(m <-chan amqp.Delivery) {
//		for ret:=range m{
//			fmt.Println(i,string(ret.Body),time.Now())
//		}
//	})
//	if err!=nil{
//		fmt.Println(i,err,time.Now())
//	}
//
//	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
//	<-forever
//}

//func TestRabbitMQ_PublishPub(t *testing.T) {
//	r,err:=NewRabbitMQPubSub("amqp://guest:guest@127.0.0.1:5672/","test")
//	if err!=nil{
//		t.Fatal(err)
//	}
//	fmt.Println("publish message.")
//	for i:=0;i<100;i++{
//		err=r.PublishPub("publish message.")
//		if err!=nil{
//			t.Fatal(err)
//		}
//	}
//	time.Sleep(1*time.Second)
//}
//
//func TestRabbitMQ_RecieveSub(t *testing.T) {
//	forever := make(chan bool)
//	go genRecieveSub(1)
//	go genRecieveSub(2)
//	go genRecieveSub(3)
//	<-forever
//}
//
//func genRecieveSub(i int)  {
//	r,err:=NewRabbitMQPubSub("amqp://guest:guest@127.0.0.1:5672/","test")
//	if err!=nil{
//		fmt.Println(i,err,time.Now())
//	}
//	err=r.RecieveSub(func(m <-chan amqp.Delivery) {
//		for ret:=range m{
//			fmt.Println(i,string(ret.Body),time.Now())
//		}
//	})
//	if err!=nil{
//		fmt.Println(i,err,time.Now())
//	}
//}

//func TestRabbitMQ_PublishRouting(t *testing.T) {
//	r,err:=NewRabbitMQWithExchangeRoutingKey("amqp://guest:guest@127.0.0.1:5672/","test1","test1")
//	if err!=nil{
//		t.Fatal(err)
//	}
//	fmt.Println("publish message.")
//	r.PublishRouting("publish message.")
//}
//
//func TestRabbitMQ_RecieveRouting(t *testing.T) {
//	forever := make(chan bool)
//	go genRecieveRouting(1)
//	go genRecieveRouting(2)
//	<-forever
//}
//
//func genRecieveRouting(i int)  {
//	r,err:=NewRabbitMQWithExchangeRoutingKey("amqp://guest:guest@127.0.0.1:5672/","test"+strconv.Itoa(i),"test"+strconv.Itoa(i))
//	if err!=nil{
//		fmt.Println(i,err,time.Now())
//	}
//	err=r.RecieveRouting(func(m <-chan amqp.Delivery) {
//		for ret:=range m{
//			fmt.Println(i,string(ret.Body),time.Now())
//		}
//	})
//	if err!=nil{
//		fmt.Println(i,err,time.Now())
//	}
//
//}

//func TestRabbitMQ_PublishTopic(t *testing.T) {
//	r,err:=NewRabbitMQWithExchangeRoutingKey("amqp://guest:guest@127.0.0.1:5672/","topic1","topic1")
//	if err!=nil{
//		t.Fatal(err)
//	}
//	fmt.Println("publish message.")
//	r.PublishRouting("publish message.")
//}

func TestRabbitMQ_RecieveTopic(t *testing.T) {
	forever := make(chan bool)
	go genRecieveTopic(2,"topic.one.*")
	go genRecieveTopic(3,"topic.one.#")
	go genRecieveTopic(4,"*.topic.three")
	<-forever
}

func genRecieveTopic(i int,topic string)  {
	r,err:=NewRabbitMQWithExchangeRoutingKey("amqp://guest:guest@127.0.0.1:5672/","topic5",topic)
	if err!=nil{
		fmt.Println(i,err,time.Now())
	}
	err=r.RecieveTopic(func(m <-chan amqp.Delivery) {
		for ret:=range m{
			fmt.Println(i,string(ret.Body),time.Now())
		}
	})
	if err!=nil{
		fmt.Println(i,err,time.Now())
	}

}


