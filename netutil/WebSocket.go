package netutil

import "github.com/gorilla/websocket"

type MyWebSocket struct {
	Ws *websocket.Conn
	Sc chan []byte
	Data *Data
}

type Hub struct {
	
}

type Data struct {
	From string
	To string
	Msg []byte
	Ip string
}

